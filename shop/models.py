from django.db import models

# Create your models here.
class Product(models.Model):
    product_id = models.AutoField
    product_name = models.CharField(max_length=50)
    catagory = models.CharField(max_length=50, default='')
    subcatagory = models.CharField(max_length=50, default='')
    price = models.IntegerField(default=0)
    desc = models.CharField(max_length=1000)
    pub_date = models.DateField()
    image = models.ImageField(upload_to='shop/images', default='')

class Contact(models.Model):
    msg_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=30, default='')
    phone = models.CharField(max_length=30, default='')
    message = models.CharField(max_length=5000)

    def __str__(self):
        return self.name
    